﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DTO
{
    /// <summary>
    /// 钉钉微应用配置
    /// </summary>
    public class DingtalkWeAppConfig
    {
        /// <summary>
        /// 微应用Id
        /// </summary>
        public string AppId { get; set; }
        /// <summary>
        /// 微应用密钥
        /// </summary>
        public string Secrect { get; set; }
    }
}
