﻿using BusinessLayer.Interface;
using DTO;
using ElasticSearchAccess;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class DebugLogManager : IDebugLogManager
    {
        private DebugLogAccess _DebugLogAccess = new DebugLogAccess();

        public void DeleteLog(string id)
        {
            _DebugLogAccess.DeleteLog(id);
        }

        public void DeleteLog(List<string> ids)
        {
            _DebugLogAccess.DeleteLog(ids);
        }

        public List<LogInfo> QueryLogInfo(LogQuery logQuery)
        {
            return _DebugLogAccess.QueryLogRequest(logQuery);
        }
    }
}
