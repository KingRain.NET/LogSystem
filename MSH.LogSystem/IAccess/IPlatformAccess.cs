﻿using DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IAccess
{
    public interface IPlatformAccess
    {
        Platform GetPlatformByAppId(string appId);

        List<Platform> QueryPlatform(PlatformQuery query);

        void AddPlatform(Platform platform);

        void DeletePlatform(List<string> ids);

        Platform GetPlatformByAppSecrect(string appId, string secrect);
    }
}
